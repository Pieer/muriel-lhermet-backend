<?php

namespace App\Http\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLQuery;

use App\Paint;

class PaintsQuery extends GraphQLQuery
{
    /**
     * Type query returns.
     *
     * @return Type
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('paint'));
    }

    /**
     * Available query arguments.
     *
     * @return array
     */
    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'title' => ['name' => 'title', 'type' => Type::string()]
        ];
    }

    /**
     * Resolve the query.
     *
     * @param  mixed  $root
     * @param  array  $args
     * @return mixed
     */
    public function resolve($root, array $args)
    {
        if(isset($args['id']))
        {
            return Paint::where('id' , $args['id'])->get();
        }
        else if(isset($args['title']))
        {
            return Paint::where('title', $args['title'])->get();
        }
        else
        {
            return Paint::all();
        }
    }
}
<?php

namespace App\Http\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLQuery;

use App\Page;

class PageQuery extends GraphQLQuery
{
    /**
     * Type query returns.
     *
     * @return Type
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('page'));
    }

    /**
     * Available query arguments.
     *
     * @return array
     */
    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'name' => ['name' => 'name', 'type' => Type::string()]
        ];
    }

    /**
     * Resolve the query.
     *
     * @param  mixed  $root
     * @param  array  $args
     * @return mixed
     */
    public function resolve($root, array $args)
    {
        if(isset($args['id']))
        {
            return Page::where('id' , $args['id'])->get();
        }
        else if(isset($args['name']))
        {
            return Page::where('name', $args['name'])->get();
        }
        else
        {
            return Page::all();
        }
    }
}
<?php

namespace App\Http\GraphQL\Types;

use GraphQL;
use App\Http\GraphQL\Connections\CollectionPaintsConnection;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLType;
use Nuwave\Lighthouse\Support\Interfaces\RelayType;

class CollectionType extends GraphQLType implements RelayType
{
    /**
     * Attributes of type.
     *
     * @var array
     */
    protected $attributes = [
        'name' => 'Collection',
        'description' => 'The collection'
    ];

    /**
     * Get model by id.
     *
     * Note: When the root 'node' query is called, this method
     * will be used to resolve the type by providing the id.
     *
     * @param  mixed $id
     * @return mixed
     */
    public function resolveById($id)
    {
        return \App\Collection::first($id);
    }

    /**
     * Type fields.
     *
     * @return array
     */
    public function fields()
    {
        return [
            'name' => [
                'type' => Type::string(),
                'description' => 'Name of the collection.',
            ],
            'featured' => [
                'type' => Type::boolean(),
                'description' => 'The collection is featured.',
            ],
            'order' => [
                'type' => Type::int(),
                'description' => 'The order in the exhibition list to display.',
            ],
            'paints' => GraphQL::connection(new CollectionPaintsConnection)->field(),
        ];
    }
}
<?php

namespace App\Http\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLType;
use Nuwave\Lighthouse\Support\Interfaces\RelayType;

use App\Http\GraphQL\Connections\PagePaintsConnection;

class PageType extends GraphQLType implements RelayType
{
    /**
     * Attributes of type.
     *
     * @var array
     */
    protected $attributes = [
        'name' => 'Page',
        'description' => 'The page'
    ];

    /**
     * Get model by id.
     *
     * Note: When the root 'node' query is called, this method
     * will be used to resolve the type by providing the id.
     *
     * @param  mixed $id
     * @return mixed
     */
    public function resolveById($id)
    {
        return \App\Page::first($id);
    }

    /**
     * Type fields.
     *
     * @return array
     */
    public function fields()
    {
        return [
            'name' => [
                'type' => Type::string(),
                'description' => 'Name of the page.',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'Title of the page.',
            ],
            'meta' => [
                'type' => Type::string(),
                'description' => 'The meta descriptions of the page.',
            ],
            'keywords' => [
                'type' => Type::string(),
                'description' => 'Keywords of the page.',
            ],
            'desc' => [
                'type' => Type::string(),
                'description' => 'The html content of the page.',
            ],
            'paints' => GraphQL::connection(new PagePaintsConnection)->field(),
        ];
    }
}
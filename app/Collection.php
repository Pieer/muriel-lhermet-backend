<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Nuwave\Lighthouse\Support\Traits\RelayConnection;

class Collection extends Model
{
    use RelayConnection;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Paints that belongs to the collection.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paints()
    {
        return $this->hasMany(Paint::class);
    }
}
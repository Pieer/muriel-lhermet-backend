<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed functions.
     *
     * @var array
     */
    protected $seeds = [
        'migrate',
        'users',
        'collections',
        'exhibitions',
        'pages',
        'paints',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeds as $seed) {
            $this->command->line("Processing: {$seed}");
            call_user_func([$this, $seed]);
        }
    }

    /**
     * (Re)Migrate tables.
     *
     * @return void
     */
    public function migrate()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        foreach(\DB::select('SHOW TABLES') as $table) {
            $table_array = get_object_vars($table);
            \Schema::drop($table_array[key($table_array)]);
        }
        $this->command->call('migrate:reset');
        $this->command->call('migrate');
        $this->command->line('Migrated tables.');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function users()
    {
        $this->command->line('Seeded users');
        // Seed user
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'schweiger.pierre@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    
    }

    public function collections()
    {
        $this->command->line('Seeded collections');

        DB::table('collections')->insert([
            'name' => 'emergence',
            'featured' => true,
            'order' => 1,
        ]);

        DB::table('collections')->insert([
            'name' => 'les_solitaires',
            'featured' => true,
            'order' => 2,
        ]);

        DB::table('collections')->insert([
            'name' => 'paysages_dechires',
            'featured' => true,
            'order' => 3,
        ]);

        DB::table('collections')->insert([
            'name' => 'geometrical_abstractions_I',
            'featured' => true,
            'order' => 4,
        ]);

        DB::table('collections')->insert([
            'name' => 'geometrical_abstractions_II',
            'featured' => false,
            'order' => 5,
        ]);

        DB::table('collections')->insert([
            'name' => 'confrontation',
            'featured' => false,
            'order' => 6,
        ]);

        DB::table('collections')->insert([
            'name' => 'crosses',
            'featured' => false,
            'order' => 7,
        ]);

        DB::table('collections')->insert([
            'name' => 'the_possible_earth',
            'featured' => false,
            'order' => 8,
        ]);

        DB::table('collections')->insert([
            'name' => 'crossing_of_the_shadows',
            'featured' => false,
            'order' => 9,
        ]);

        DB::table('collections')->insert([
            'name' => 'back_worlds',
            'featured' => false,
            'order' => 10,
        ]);

        DB::table('collections')->insert([
            'name' => 'the_watchers',
            'featured' => false,
            'order' => 11,
        ]);
    }

    public function exhibitions()
    {
        $this->command->line('Seeded exhibitions');

        DB::table('exhibitions')->insert([
            'title' => 'Exposition Swiss Art Space, Lausanne',
            'year' => 2017
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Open Space, Sète',
            'year' => 2017
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon International d’art Contemporain SIAC, Marseille',
            'year' => 2017
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Egregore, Marmande',
            'year' => 2017
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon d’art contemporain Viv’art, Albi',
            'year' => 2016
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition Galerie Aude Guirauden, Toulouse',
            'year' => 2016
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon “Comparaisons” à Royan',
            'year' => 2015
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon “Comparaisons” au Grand Palais à Paris',
            'year' => 2015
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition permanente, Galerie Aude Guirauden, Toulouse',
            'year' => 2015
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition permanente, Galerie Aude Guirauden, Toulouse',
            'year' => 2014
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition au musée des Beaux-Arts de Gaillac',
            'year' => 2014
        ]);
        
        DB::table('exhibitions')->insert([
            'title' => 'Salon “Comparaisons” au Grand Palais à Paris',
            'year' => 2014
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition, Espace Lhomond, Paris',
            'year' => 2013
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition permanente, Galerie Aude Guirauden, Toulouse',
            'year' => 2013
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Exposition collective au Centre d’art contemporain de Bédarieux',
            'year' => 2013
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie 3F, Paris',
            'year' => 2012
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Aude Guirauden, Toulouse',
            'year' => 2012
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie des Carmes, Toulouse',
            'year' => 2012
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie L’étangd’Art, Bages',
            'year' => 2012
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Eglise St-Michel, Lescure d’Albigeois',
            'year' => 2012
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie des Carmes, Toulouse',
            'year' => 2011
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie El Indalo, Limoux',
            'year' => 2011
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Espace Gibert, Lézignan',
            'year' => 2011
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie El Indalo, Limoux',
            'year' => 2010
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon d’art contemporain, Arts 2, Bruxelles',
            'year' => 2010
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie L’étangd’Art, Bages',
            'year' => 2010
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon d’art contemporain, Viv’art, Albi',
            'year' => 2010
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie des Carmes, Toulouse',
            'year' => 2009
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Artevistas, Albi et Barcelone',
            'year' => 2009
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie A7, Auvillar',
            'year' => 2008
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie de l’Echarpe, Toulouse',
            'year' => 2007
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Salon d’art contemporain, Bordeaux',
            'year' => 2005
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie de l’Echarpe, Toulouse',
            'year' => 2004
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Batik’Art, Barcelone',
            'year' => 2003
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Puzzl’Art, Castres',
            'year' => 2002
        ]);

        DB::table('exhibitions')->insert([
            'title' => 'Galerie Fusion, Toulouse',
            'year' => 2000
        ]);

    }

    public function pages()
    {
        $this->command->line('Seeded pages');

        DB::table('pages')->insert([
            'name' => 'home',
            'title' => 'Home',
            'meta' => 'This is home meta test',
            'keywords' => 'home keyword test',
            'desc' => 'Muriel Lhermet website',
        ]);

        DB::table('pages')->insert([
            'name' => 'collections',
            'title' => 'Collections',
            'meta' => 'This is collections meta test',
            'keywords' => 'collections keyword test',
            'desc' => 'Muriel Lhermet website',
        ]);

        DB::table('pages')->insert([
            'name' => 'artist',
            'title' => 'Artist statment',
            'meta' => 'This is artist meta test',
            'keywords' => 'artist keyword test',
            'desc' => 'My work takes the form of carefully constructed abstractions, built up from multiple thin layers of vibrant pigments which define shapes, spaces, intermediary worlds – limbos in which opposing principles come together: light and dark, dense and volatile, earthy and ethereal…<br><br>I create spaces, cocoons from which life may emerge or perhaps sarcophagi, precious stones harbouring palpitations of light…<br><br>These otherworlds are connected by sinuous lines, life-giving veins of colour, arteries linking one being to another…<br><br>What I seek is the vibration of colour – whether it be brightly radiant or deep and intimate – and what each piece of work generates beyond the canvas, in the eye of the beholder…',
        ]);

        DB::table('pages')->insert([
            'name' => 'workshop',
            'title' => 'The workshop',
            'meta' => 'This is workshop meta test',
            'keywords' => 'workshop keyword test',
            'desc' => 'I opened my painting studio in 2007 at Cordes-sur-Ciel, a medieval hill-top village whose ancient stones inspire my painting both in terms of structure and in the mineral nature of the pigments I use.<br><br>The village has long been home to a great many artists. Like a stone vessel, Cordes-sur-Ciel is one of the best-loved villages in France and a favourite with visitors from abroad',
        ]);

        DB::table('pages')->insert([
            'name' => 'exhibitions',
            'title' => 'Exhibitions',
            'meta' => 'This is exhibitions meta test',
            'keywords' => 'exhibitions keyword test',
            'desc' => 'Muriel Lhermet website',
        ]);

    }

    public function paints()
    {
        factory(App\Paint::class, 50)->create();
        $this->command->line('Seeded paints');
    }
}

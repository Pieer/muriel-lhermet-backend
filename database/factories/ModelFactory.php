<?php

$factory->define(App\Paint::class, function (Faker\Generator $faker) {
  $pagesIDs = DB::table('pages')->pluck('id')->all();
  $collectionsIDs = DB::table('collections')->pluck('id')->all();

  return [
    'title' => $faker->sentence($nbWords = 2, $variableNbWords = true),
    'dimension' => $faker->numberBetween($min = 100, $max = 400).'cm x '.$faker->numberBetween($min = 200, $max = 600).'cm',
    'page_id' => $faker->randomElement($pagesIDs),
    'collection_id' => $faker->randomElement($collectionsIDs)
  ];
});
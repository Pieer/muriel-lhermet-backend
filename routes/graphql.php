<?php

GraphQL::schema()->group(['namespace' => 'App\\Http\\GraphQL'], function () {
    GraphQL::schema()->type('user', 'Types\\UserType');
    GraphQL::schema()->type('page', 'Types\\PageType');
    GraphQL::schema()->type('paint', 'Types\\PaintType');
    GraphQL::schema()->type('collection', 'Types\\CollectionType');
    GraphQL::schema()->type('exhibition', 'Types\\ExhibitionType');

    GraphQL::schema()->query('viewer', 'Queries\\ViewerQuery');
    GraphQL::schema()->query('collections', 'Queries\\CollectionsQuery');
    GraphQL::schema()->query('exhibitions', 'Queries\\ExhibitionsQuery');
    GraphQL::schema()->query('pages', 'Queries\\PageQuery');
    GraphQL::schema()->query('paints', 'Queries\\PaintsQuery');
});